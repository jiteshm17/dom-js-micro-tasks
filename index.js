function createElementValue(element,value){
    var tag = document.createElement(element);
    tag.innerHTML = value;
    return tag
}

function createElement(element){
    var tag = document.createElement(element);
    return tag
}


function showCoordinates(event){
    span = createElement('span');
    span.id = 'mouse-coordinates';
    span.className = 'text-center';
    span.style.color = 'red';
    element = document.getElementById('show-coordinates');  
    x = event.clientX
    y = event.clientY; 
    span.innerHTML = x + " " + y;
    document.body.append(span);
    setTimeout(function(){document.getElementById('mouse-coordinates').remove()}, 100);
    
}

function changeColor(){
    element = document.getElementById('show-coordinates');
    element.style.color = 'blue'
}

function restoreColor(){
    element = document.getElementById('show-coordinates');
    element.style.color = 'black'
}


function countCharacters(){
    element = document.getElementById('counter')
    text = element.value;
    document.getElementById('num-chars').value = text.length.toString();
    console.log(text.length);
}
